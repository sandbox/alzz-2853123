CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Usage
 
 
 INTRODUCTION
------------

Simple link field formatter wich adds different css classes depending on the social link url.
It works out of the box for:
 - Facebook
 - Twitter
 - Google +
 - Youtube
 - Linkedin
 - Email
 
Also adds the link url domain as a class.


REQUIREMENTS
------------

None

 
INSTALLATION
------------

Media: Olympic Channel can be installed via the standard Drupal installation process
(http://drupal.org/node/895232).


USAGE
-----

This module only adds the css class from link url, other modules bring similar functionality, check them out!
Social Media Link Formatter: https://www.drupal.org/project/socialmedia_link_formatter

