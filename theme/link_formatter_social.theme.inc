<?php

/**
 * @file
 * Link formatter social theme functions.
 */

/**
 * Themes link field into social networks links.
 */
function theme_link_formatter_social($variables) {

  $url = check_url($variables['link']['url']);
  $host = parse_url($url, PHP_URL_HOST);
  $text = $variables['link']['title'];

  $link_options = array(
    'absolute' => TRUE,
    'attributes' => array(
      'class' => array('social-link'),
      'target' => '_blank',
      'title' => t('Link'),
    ),
  );

  $link_options['attributes']['class'][] = drupal_html_class(str_replace('.', '-', $host));
  if (($host_wwwless = preg_replace("/^www\./i", '', $host, -1, $count)) && $count) {
    $link_options['attributes']['class'][] = drupal_html_class(str_replace('.', '-', $host_wwwless));
  }

  // Identify social networks links.
  // @TODO: If recoginize social networs theres no need to check for all, SKIP.
  // @TODO: Add more "social links" like instagram ...
  preg_match('#https?\://(?:www\.)?facebook\.com/(\d+|[A-Za-z0-9\.]+)/?#', $url, $facebook);
  preg_match('#https?\://(?:www\.)?twitter\.com/(\d+|[A-Za-z0-9\.]+)/?#', $url, $twitter);
  preg_match('#https?\://(?:www\.)?plus.google\.com/(\d+|[A-Za-z0-9\.]+)/?#', $url, $google);
  preg_match('#https?\://(?:www\.)?youtube\.com/user/(\d+|[A-Za-z0-9\.]+)/?#', $url, $youtube);
  preg_match('#https?\://(?:www\.)?linkedin\.com/in/(\d+|[A-Za-z0-9\.]+)/?#', $url, $linkedin);
  $is_email = valid_email_address($url);


  if (!empty($facebook)) {
    $link_options['attributes']['title'] = $facebook[1];
    $link_options['attributes']['class'][] = 'facebook';
  }
  elseif (!empty($twitter)) {
    $link_options['attributes']['title'] = '@' . $twitter[1];
    $link_options['attributes']['class'][] = 'twitter';
  }
  elseif (!empty($google)) {
    $link_options['attributes']['title'] = 'google+';
    $link_options['attributes']['class'][] = 'google';
  }
  elseif (!empty($youtube)) {
    $link_options['attributes']['title'] = 'YouTube - ' . $youtube[1];
    $link_options['attributes']['class'][] = 'youtube';
  }
  elseif (!empty($linkedin)) {
    $link_options['attributes']['title'] = 'linkedin';
    $link_options['attributes']['class'][] = 'linkedin';
  }
  elseif ($is_email) {
    $link_options['attributes']['title'] = t('Email');
    $link_options['attributes']['class'][] = 'email';
    $url = 'mailto:' . $url;
    unset($link_options['attributes']['target']);
  }
  else {
    $link_options['attributes']['title'] = $url;
    $link_options['attributes']['class'][] = 'other';
  }

  $link = l($text, $url, $link_options);

  return $link;
}
